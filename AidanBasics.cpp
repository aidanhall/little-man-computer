#include "AidanBasics.hpp"

namespace AidanBasics
{
	std::vector<std::string> make_tokens(std::string sentence) {
		using namespace std;
		/* // TODO: Understand this nonsense. */
		/* istringstream line_stream (sentence); */
		/* return { istream_iterator<string>{line_stream}, */
		/* 	istream_iterator<string>{} }; */

		vector<string> tokens;
		string current;
		for (auto character : sentence) {
			if (std::isspace(character)) {
				if (not current.empty()) {
					tokens.push_back(current);
					current.clear();
				}
			} else {
				current.push_back(character);
			}
		}

		// Push final string:
		if (not current.empty()) {
			tokens.push_back(current);
		}

		return tokens;
	}

	inline void makelowercase(std::string& source) {
		std::transform(source.begin(), source.end(),
			       source.begin(),
			       [](char c) -> char {
				       if (std::isalpha(c))
					       return std::tolower(c);
				       else
					       return c;
			       });
	}

	inline void makeuppercase(std::string& source) {
		std::transform(source.begin(), source.end(),
			       source.begin(),
			       [](char c) -> char {
				       if (std::isalpha(c))
					       return std::toupper(c);
				       else
					       return c;
			       });
	}

	std::string lowercase(const std::string& source) {
		std::string lower = source;
		makelowercase(lower);
		return lower;
	}

	std::string uppercase(const std::string& source) {
		std::string upper = source;
		makeuppercase(upper);
		return upper;
	}


}
