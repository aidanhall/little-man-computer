/*
    _    _     _             _       ____            _
   / \  (_) __| | __ _ _ __ ( )___  | __ )  __ _ ___(_) ___ ___
  / _ \ | |/ _` |/ _` | '_ \|// __| |  _ \ / _` / __| |/ __/ __|
 / ___ \| | (_| | (_| | | | | \__ \ | |_) | (_| \__ \ | (__\__ \
/_/   \_\_|\__,_|\__,_|_| |_| |___/ |____/ \__,_|___/_|\___|___/

A whole bunch of basic stuff.
*/


#pragma once
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>


namespace AidanBasics {


	template <typename T> class Bounds {
		public:
			const T lower;
			const T upper;
			Bounds(T lower, T upper) :
				lower{lower},
				upper{upper} {}

			inline bool in_bounds(T& item) {
				return this->lower <= item and item <= this->upper;
			}

			inline bool operator()(T& item) {
				return in_bounds(item);
			}
	};


	std::vector<std::string> make_tokens(std::string sentence);

	void makelowercase(std::string& source);
	void makeuppercase(std::string& source);

	std::string lowercase(const std::string& source);
	std::string uppercase(const std::string& source);
};
