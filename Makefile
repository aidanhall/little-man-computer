CXX = clang++
CXXFLAGS = -std=c++20 -g
OUTPUT_OPTION = -MMD -MP -o $@

CXXFILES = $(wildcard *.cpp)
OBJS = $(CXXFILES:.cpp=.o)
DEPS = $(CXXFILES:.cpp=.d)

LittleManComputer: ${OBJS}
	${CXX} -o $@ ${OBJS}

clean:
	rm -f ${OBJS} ${DEPS}
