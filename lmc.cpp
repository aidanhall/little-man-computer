#include "lmc.hpp"
#include <iostream>

namespace littleMan
{

	namespace Assembler {

		const std::unordered_map<std::string, Memory::Item> mnemonic_map = {
			{ "add", Instruction::InADD },
			{ "sub", Instruction::InSUB },
			{ "sta", Instruction::InSTA },
			{ "lda", Instruction::InLDA },
			{ "bra", Instruction::InBRA },
			{ "brz", Instruction::InBRZ },
			{ "brp", Instruction::InBRP },
			{ "inp", Instruction::InINP },
			{ "out", Instruction::InOUT },
			{ "dat", Instruction::InDAT },
			{ "hlt", Instruction::InHLT },
			{ "cob", Instruction::InCOB },
		};

		Memory::Values assemble(const std::vector<std::string>& program_source) {
			DecomposedLines decomposed = decompose_lines(program_source);

			auto label_refs = get_label_references(decomposed.labels);
			auto opcodes = mnemonics_to_opcodes(decomposed.mnemonics);
			auto operands = format_operands(decomposed.operands, label_refs);

			return format_instructions(opcodes, operands);
		}

		DecomposedLines decompose_lines(
			const std::vector<std::string>& program_source) {

			DecomposedLines decomposition;

			for (auto line : program_source) {
				decomposition.add_line(make_tokens(line));
			};

			return decomposition;
		}

		LabelReferences get_label_references(
			const std::vector<std::string>& labels
			) {
			LabelReferences references;
			for (Memory::Index lineno = 0; lineno < labels.size(); ++lineno) {
				if (!(labels[lineno].empty()
				      or references.contains(labels[lineno]))) {
					references[labels[lineno]] = lineno;
				}
			}

			return references;
		}


		Memory::Values mnemonics_to_opcodes(
			const std::vector<std::string>& mnemonics) {

			Memory::Values opcodes;

			for (Memory::Index addr = 0; addr < opcodes.size()
				     and addr < mnemonics.size(); ++addr) {
				std::string mnemonic = lowercase(mnemonics[addr]);
				if (mnemonic_map.contains(mnemonic)) {
					opcodes[addr] = mnemonic_map.at(mnemonic);
				} else {
					opcodes[addr] = Instruction::InHLT;
				}
			}

			return opcodes;
		}



		Memory::Values format_operands(
			const std::vector<std::string>& operand_strings,
			const LabelReferences& label_refs) {

			Memory::Values operands;

			for (Memory::Index i = 0; i < operand_strings.size()
				     and i < Memory::number_of_values; ++i) {
				// CURSE YOU STROUSTRUP
				if (label_refs.contains(operand_strings[i])) {
					/* std::cerr << "Found label: " << operand_strings[i] << ", resolving...\n"; */
					operands[i] = label_refs.at(operand_strings[i]);
				} else {
					try {
						operands[i] = std::stoi(operand_strings[i]);
						/* std::cerr << "We got a number: " << operands[i] << " from " << operand_strings[i] << std::endl; */
					} catch (std::invalid_argument) {
						operands[i] = Instruction::InHLT;
					} catch (std::out_of_range) {
						operands[i] = Instruction::InHLT;
					}
				}
			}

			return operands;
		}


		Memory::Values format_instructions(
			const Memory::Values& opcodes,
			const Memory::Values& operands) {
			Memory::Values instructions;

			std::transform(opcodes.begin(), opcodes.end(),
				       operands.begin(),
				       instructions.begin(),
				       [](auto& opcode, auto& operand)->auto
					       { return opcode + operand; });

			return instructions;
		}


		inline void DecomposedLines::add_line(const std::vector<std::string>& tokens) {
			switch (tokens.size()) {
			case 0:
				this->push_back("", "", "");
				break;
			case 1:
				this->push_back("", tokens[0], "");
				break;
			case 2:
				if (mnemonic_map.contains(lowercase(tokens[0]))) {
					this->push_back("", tokens[0], tokens[1]);
				} else {
					this->push_back(tokens[0], tokens[1], "");
				}
				break;
			default:
				this->push_back(tokens[0], tokens[1], tokens[2]);
				break;
			}
			/* std::cerr << "Dec line: label: " << this->labels.back() */
			/*	<< "\tinst: " << this->mnemonics.back() */
			/*	<< "\toper: " << this->operands.back() */
			/*	<< std::endl; */
		}

		void DecomposedLines::push_back(
			std::string label, std::string mnemonic, std::string operand) {
			labels.push_back(label);
			mnemonics.push_back(mnemonic);
			operands.push_back(operand);
		}
	}

	void Memory::validate_values(Values& values) {
		std::transform(values.begin(), values.end(), values.begin(),
			       [&](auto& item) -> auto {
				       return (item_bounds.in_bounds(item))? item : Instruction::InHLT; });
	}

	inline void Memory::set_value(Index index, Item value) {
		if (index_bounds.in_bounds(index)
		    and item_bounds.in_bounds(value)) {
			values[index] = value;
		}
	}

	void Memory::set_values(Memory::Values values) {
		validate_values(values);
		this->values = values;
	}


	Memory::Item Memory::overflowed(Memory::Item value) {
		// Bounds are inclusive, so offsets are added accordingly.
		if (value > item_bounds.upper) {
			return item_bounds.lower
				+ value % (item_bounds.upper+1-item_bounds.lower);
		} else if (value < item_bounds.lower) {
			return item_bounds.upper
				+ (1 + value) % (item_bounds.lower-item_bounds.upper-1);
		} else {
			return value;
		}
	}

	const Memory& Computer::get_memory(void) {
		return memory;
	}

	Memory::Index Computer::jump(Memory::Index address) {
		flags.jump = true;
		if (memory.index_bounds.in_bounds(address)) {
			return (program_counter = address);
		} else {
			return memory.index_bounds.upper + 1;
		}
	}

	Memory::Index Computer::get_program_counter(void) {
		return program_counter;
	}

	void Computer::tick() {
		switch (state) {
		case OperatingState::Running: {
			Memory::Item instruction = fetch();
			execute(instruction);
			if (not flags.jump) {
				program_counter++;
			} else {
				flags.jump = false;
			}
		} break;

		case OperatingState::WaitingForInput: {
			if (not input_buffer.empty()) {
				/* std::cerr << "resuming\n"; */
				set_accumulator(input_buffer.front());
				/* std::cerr << "Wuss poppin'?\n"; */
				input_buffer.pop();
				/* std::cerr<< "ibl: " << input_buffer.size() << std::endl; */
				state = OperatingState::Running;
			}
		} break;

		case OperatingState::Halted:
			break;
		}
	}

	Memory::Item Computer::fetch() {
		return memory.get_values().at(program_counter);
	}

	void Computer::execute(Memory::Item instruction) {
		/* std::cerr << "ACC: " << accumulator << " Executing line: " << 1+program_counter << ":\t" << instruction << std::endl; */
		auto dec_instr = decode(instruction);
		switch (dec_instr.opcode) {
		case Instruction::InADD:
			/* std::cerr << "Add" << memory.get_values().at(dec_instr.operand) << std::endl; */
			set_accumulator(accumulator
					+ memory.get_values().at(dec_instr.operand));
			break;
		case Instruction::InSUB:
			/* std::cerr << "Sub" << memory.get_values().at(dec_instr.operand) << std::endl; */
			set_accumulator(accumulator
					- memory.get_values().at(dec_instr.operand));
			break;
		case Instruction::InSTA:
			memory.set_value(dec_instr.operand, accumulator);
			break;
		case Instruction::InLDA:
			set_accumulator(memory.get_values().at(dec_instr.operand));
			break;
		case Instruction::InBRA:
			jump(dec_instr.operand);
			break;
		case Instruction::InBRZ:
			if (accumulator == 0)
			{
				/* std::cerr << "Jumping\n"; */
				jump(dec_instr.operand);
			}
			break;
		case Instruction::InBRP:
			if (not flags.negative)
				jump(dec_instr.operand);
			break;
		case Instruction::InIOP:
			/* std::cerr << "IO babey\n"; */
			switch (dec_instr.operand) {
			case IOOperands::IOOut:
				/* std::cerr << "Output: "; */
				output_buffer.push(accumulator);
				break;
			case IOOperands::IOInp:
				if (not input_buffer.empty()) {
					/* std::cerr << "We already had a value: " << input_buffer.front() << std::endl; */
					set_accumulator(input_buffer.front());
					input_buffer.pop();
				} else {
					/* std::cerr << "No input; waiting...\n"; */
					state = OperatingState::WaitingForInput;
				}
				break;
			default:
				/* std::cerr << "Aw piss. This shouldn't happen!\n"; */
				break;
			}
			break;
		case Instruction::InHLT:
		default:
			/* std::cerr << "Time to gOo!\n"; */
			state = OperatingState::Halted;
			break;
		}
	}


	void Computer::set_accumulator(Memory::Item value) {
		flags.negative = (value < 0);
		/* accumulator = memory.overflowed(value); */
		accumulator = value;
	}

	void Computer::input(Memory::Item value) {
		input_buffer.push(value);
	}

	Memory::Item Computer::output() {
		Memory::Item ret_val;
		if (not output_buffer.empty()) {
			ret_val = output_buffer.front();
			output_buffer.pop();
		} else {
			ret_val = 0;
		}
		return ret_val;
	}
	struct DecomposedInstruction Computer::decode(Memory::Item instruction) {
		auto split = std::div(instruction, opcode_shift);

		Memory::Index operand = split.rem;
		enum Instruction opcode = static_cast<enum Instruction>(split.quot * opcode_shift);

		if (not memory.index_bounds.in_bounds(operand)) {
			opcode = Instruction::InHLT;
		}

		return {opcode, operand};
	}

}
