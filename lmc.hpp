#pragma once
#include <cstdint>
#include <array>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
#include <utility>
#include <unordered_map>
#include <queue>
#include <bitset>
#include "AidanBasics.hpp"

/* namespace lmc = littleMan; recommended for sanity */
namespace littleMan
{
	using namespace AidanBasics;

	enum class OperatingState {
		Halted,
		Running,
		WaitingForInput,
	};

	const size_t opcode_shift = 100;

	enum IOOperands {
		IOInp = 1,
		IOOut = 2,
	};
	enum Instruction {
		InADD = 1 * opcode_shift,
		InSUB = 2 * opcode_shift,
		InSTA = 3 * opcode_shift,
		InLDA = 5 * opcode_shift,
		InBRA = 6 * opcode_shift,
		InBRZ = 7 * opcode_shift,
		InBRP = 8 * opcode_shift,
		InIOP = 9 * opcode_shift,
		InINP = InIOP + IOOperands::IOInp,
		InOUT = InIOP + IOOperands::IOOut,
		InHLT = 0,
		InCOB = InHLT,
		InDAT = 0,
	};
	


	class Memory {
		public:
			/* Signed because accumulator can be and we do maths
			 * with memory and accumulator
			 */
			using Item = int16_t;
			using Index = uint16_t;

			const static size_t number_of_values = 100;

			using Values = std::array<Item, number_of_values>;

			Bounds<Index> index_bounds;
			Bounds<Item> item_bounds;


			Memory() :
				index_bounds {0, number_of_values-1},
					     item_bounds {-999, 999}
			{}

			Memory(Values values) : Memory() {
				set_values(values);
			}

			Memory(Memory& other) : Memory() {
				set_values(other.get_values());
			}

			void set_value(Index index, Item value);
			/* void set_values(std::vector<std::pair<Index, Item>>& settings); */
			void set_values(Memory::Values values);

			void validate_values(Values& values);

			const Memory::Values& get_values() { return values; }

			Item overflowed(Item value);

		private:
			Values values;
	};

	namespace Assembler {

		struct DecomposedLines {
			std::vector<std::string> labels;
			std::vector<std::string> mnemonics;
			std::vector<std::string> operands;

			inline void add_line(const std::vector<std::string>& tokens);
			void push_back(std::string label, std::string mnemonic, std::string operand);
		};

		using LabelReferences = std::unordered_map<std::string, Memory::Index>;

		Memory::Values assemble(const std::vector<std::string>& program_source);
		DecomposedLines decompose_lines(
				const std::vector<std::string>& program_source);
		LabelReferences get_label_references(
				const std::vector<std::string>& labels);
		Memory::Values format_operands(
				const std::vector<std::string>& operand_strings,
				const LabelReferences& label_refs);
		Memory::Values mnemonics_to_opcodes(
				const std::vector<std::string>& mnemonics);
		Memory::Values format_instructions(
				const Memory::Values& opcodes,
				const Memory::Values& operands);
	}

	struct DecomposedInstruction {
		enum Instruction opcode;
		Memory::Index operand;
	};

	class Computer {
		public:
			// Public state
			OperatingState state = OperatingState::Halted;

			// Initialiser
			Computer(const std::vector<std::string>& program_source) :
				initial_values { Assembler::assemble(program_source) }
			{start();}


			// Fetch, Decode, Execute cycle
			void tick();
			Memory::Item fetch();
			struct DecomposedInstruction decode(Memory::Item item);
			void execute(Memory::Item instruction);

			// State pokers
			void reset() {
				memory.set_values(initial_values);
				program_counter = 0;
				accumulator = 0;
				flags.negative = false;
				flags.jump = false;
			}

			void start() {
				reset();
				state = OperatingState::Running;
			}

			// Setters
			Memory::Index jump(Memory::Index address);
			void set_accumulator(Memory::Item value);
			void input(Memory::Item value);

			// Getters
			Memory::Index get_program_counter(void);
			const Memory& get_memory(void);
			inline bool get_negative_flag() { return flags.negative; }
			inline bool has_output() { return not output_buffer.empty(); }
			Memory::Item output();

			std::queue<Memory::Item> input_buffer;
			std::queue<Memory::Item> output_buffer;

		private:
			Memory memory;
			Memory::Values initial_values;
			Memory::Index program_counter;
			Memory::Item accumulator;
			struct {
				uint8_t negative:1;
				uint8_t jump:1;
			} flags;

	};

}
