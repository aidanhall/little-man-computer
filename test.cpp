#include "lmc.hpp"
#include <fstream>
#include <iostream>

int main(int argc, char* argv[]) {
	using namespace std;
	using namespace littleMan;

	const string prompt = "> ";
	vector<string> source;
	fstream source_file;
	if (argc > 1) {
		source_file.open(argv[argc-1]);
	}

	for (string line; getline(source_file, line); ) {
		source.push_back(line);
	}

	source_file.close();

	Computer com { source };

	while (not (com.state == OperatingState::Halted)) {
		// Tick
		com.tick();
		// Get input
		if (com.state == OperatingState::WaitingForInput) {
			Memory::Item inval;
			string in_string;
			bool converted = false;
			do {
				std::cerr << prompt;
				std::getline(std::cin, in_string);

				try {
					inval = std::stoi(in_string);
					converted = true;
				} catch (std::invalid_argument) {
					continue;
				} catch (std::out_of_range) {
					continue;
				}
			} while (not converted);
			com.input(inval);
		}
		// Send output
		if (com.has_output()) {
			cout << com.output() << endl;
		}
	}

}
